#!/bin/bash

#set -x

export PATH=$PATH:$(pwd)


filename="z_n_final"


N=(5 10 22 30 40 50 60 )
S=(100 1000 10000 100000 1000000 10000000 100000000)

for (( i=0; i < ${#N[@]}; i++))
    do
      main_c $filename ${N[i]} ${S[i]}
    done


