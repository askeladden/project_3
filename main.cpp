#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <cstdlib>
#include <stdio.h>
#define   ZERO       1.0E-10
#include <omp.h>
#define NUM_THREADS 8
#define PI 3.14159
#define EPS 3.0e-14
#define MAXIT 10


using namespace std;


void gauleg(double, double, double *, double *, int);
void gauss_laguerre(double *, double *, int, double);
double gammln(double);
double int_function(double, double, double, double, double, double, double, double);
double int_function_orig(double, double, double, double, double, double);
double int_function_sphere(double, double, double, double, double, double);
double fx_sphere_mc_imp_sampl(double, double, double, double, double, double);

int main(int argc, char *argv[])
{

     int N; // number of integration points
     double a, b;  // integration limits
     //clock_t timer_start, timer_finish; 
     struct timeval start, end;
     char *filename;
//   set up the mesh points and weights
     a=-2.7;
     b=4;
     //N=20;
     N=atof(argv[2]);
     double calc_val;
     calc_val=5*PI*PI/(16.0*16.0);
     filename = argv[1];
// array for integration points and weights using Legendre polynomials
     double *x = new double [N];
     double *w = new double [N];
     gettimeofday(&start, NULL);
     //timer_start = clock();
     gauleg(a, b, x, w, N);
//   evaluate the integral with the Gauss-Legendre method
//   Note that we initialize the sum
     double int_gauss = 0.;
     cout << "  The number of threads available    = " << omp_get_max_threads ( )  <<  endl;
     cout << "  The number of processors available = " << omp_get_num_procs ( ) << endl;
     omp_set_num_threads(NUM_THREADS);
//   six-double loops
# pragma omp parallel for reduction(+:int_gauss) //private(i,j,k,l,m,n)
     for (int i=0;i<N;i++){
        for (int j = 0;j<N;j++){
            for (int k = 0;k<N;k++){
                for (int l = 0;l<N;l++){
                    for (int m = 0;m<N;m++){
                        for (int n = 0;n<N;n++){

        int_gauss+=w[i]*w[j]*w[k]*w[l]*w[m]*w[n]*int_function_orig(x[i],x[j],x[k],x[l],x[m],x[n]);
         }}}}}
      }
      //timer_finish = clock();
      gettimeofday(&end, NULL);
     // cout <<"Time Legandre dekart:"<<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;
     //cout <<"Gauss-Legendre method"<<int_gauss<<endl;

     ofstream ofile;
     ofile.open(filename, ios::app);
     
     ofile <<"Gauss-Legendre method"<<endl;
     ofile <<"Number of integration points = "<<N<<endl;
     ofile <<"Integral = " << int_gauss << endl;
     ofile << "Error = " << (abs(int_gauss - calc_val)/calc_val)*100 << endl;
     //ofile <<"Time Legandre dekart:"<<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;     
     ofile <<"Time Legandre dekart:"<<((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6 << endl; 
     ofile <<"================================="<<endl;
     ofile.close();

 
     delete [] x;
     delete [] w;    

////////////////////////Laguere polinomial///////////////////////////
    
    double *x_theta = new double [N];
    double *w_theta = new double [N];
    double *x_phi = new double [N];
    double *w_phi = new double [N];
    double *x_r = new double [N+1];
    double *w_r = new double [N+1];

    double alf = 2.;   // for lagurre
    //timer_start = clock();
    gettimeofday(&start, NULL);
    double sum, int_gauss_imp;
    sum=0.;
    int_gauss_imp=0.;
//   set up the mesh points and weights
    gauleg(0, PI, x_theta, w_theta, N/3);
    gauleg(0, 2.*PI, x_phi, w_phi, N/3);
    gauss_laguerre(x_r, w_r, N, alf);

    omp_set_num_threads(NUM_THREADS);

# pragma omp parallel for reduction(+:int_gauss_imp) 
 
   for (int i=1;i<=N;i++){
	for (int j = 1;j<=N;j++){
	    for (int k = 0;k<N;k++){
		for (int l = 0;l<N;l++){
		    for (int m = 0;m<N;m++){
			for (int n = 0;n<N;n++){
			    int_gauss_imp+=w_r[i]*w_r[j]*w_theta[k]*w_theta[l]*w_phi[m]*w_phi[n]
				*int_function_sphere(x_r[i], x_theta[k], x_phi[m], x_r[j], x_theta[l],x_phi[n]);
       	}}}}}
    }
 
     sum += int_gauss_imp; 
     sum *=1.0/1024;
     //timer_finish = clock();
     gettimeofday(&end, NULL);
     //cout <<"Gauss-Laguere method"<<sum<<endl;
     ofile.open(filename, ios::app);

     ofile <<"Gauss-Legendre-Laguere improved method"<<endl;
     ofile <<"Number of integration points = "<<N<<endl;
     ofile <<"Integral = " << sum << endl;
     ofile <<"Error = " << (abs(sum - calc_val)/calc_val)*100 << endl;
     //ofile <<"Time Gauss-Legendre-Laguere improved:"<<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;
     ofile <<"Time Gauss-Legendre-Laguere improved:"<<((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6 << endl;
     ofile <<"================================="<<endl;
     ofile.close();

     delete [] x_theta;
     delete [] w_theta;
     delete [] x_phi;
     delete [] w_phi;
     delete [] x_r;
     delete [] w_r;


////////////////////////////////////Monte Carlo brute fource//////////////////////////////////////////////

     //timer_start = clock();
     gettimeofday(&start, NULL);
     int S; //number of Monte-Carlo samples
     double invers_period = 1./RAND_MAX; // initialise the random number generator
     srand(time(NULL));
     double int_MC, sum_sigma, fx_decart, standart_deviation;
     int_MC = fx_decart = sum_sigma = 0;
     //S=100000000;
     S = atof(argv[3]);
#pragma omp parallel for reduction(+:int_MC, sum_sigma)
     for (int i=0;i<S;i++){
         double x1 = (double(rand())*invers_period)*(b-a)+a; 
         double x2 = (double(rand())*invers_period)*(b-a)+a; 
         double y1 = (double(rand())*invers_period)*(b-a)+a; 
         double y2 = (double(rand())*invers_period)*(b-a)+a; 
         double z1 = (double(rand())*invers_period)*(b-a)+a; 
         double z2 = (double(rand())*invers_period)*(b-a)+a; 
         fx_decart = int_function_orig(x1,x2,y1,y2,z1,z2);
         int_MC += fx_decart;
         sum_sigma += fx_decart*fx_decart;
     }
     double int_MC_st;
     int_MC_st=int_MC/S;
     int_MC *= pow((b-a),6)/S; //Jacobi and weights
     //timer_finish = clock();
     gettimeofday(&end, NULL);
     standart_deviation=sqrt(sum_sigma/(double)S - pow(int_MC_st,2));
    
     ofile.open(filename, ios::app);
     ofile <<"Monte Carlo brute fource"<<endl;
     ofile <<"Number of integration points = "<<S<<endl;
     ofile <<"Integral = " << int_MC << endl;
     ofile <<"Standard deviation = " <<standart_deviation<< endl;
     //ofile <<"Time Monte Carlo brute fource:"<<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;
     ofile <<"Time Monte Carlo brute fource: "<<((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6 << endl;
     ofile << "Error = " << (abs(int_MC - calc_val)/calc_val)*100 << endl;
     ofile <<"================================="<<endl;
     ofile.close();

////////////////////////Monte Carlo importance sampling/////////////////////////////////



     gettimeofday(&start, NULL);
     double int_MC_imp_sampl, fx_mc_imp_sampl, sum_sigm, ro1, theta1, fi1, ro2, theta2, fi2; 
     int_MC_imp_sampl=0;
     sum_sigm=0;
     //double invers_period = 1./RAND_MAX; // initialise the random number generator
     srand(time(NULL));

#pragma omp parallel for reduction(+:int_MC_imp_sampl, sum_sigm)

     for (int i=0;i<S;i++){
          ro1 = -log(1-(double(rand())*invers_period));
          ro2 = -log(1-(double(rand())*invers_period));
          theta1 = double(rand())*invers_period*PI;
          theta2 = double(rand())*invers_period*PI;
          fi1 = double(rand())*invers_period*PI*2.;
          fi2 = double(rand())*invers_period*PI*2.;
           
          fx_mc_imp_sampl = fx_sphere_mc_imp_sampl(ro1, theta1, fi1, ro2, theta2, fi2);
          sum_sigm += fx_mc_imp_sampl*fx_mc_imp_sampl;
          int_MC_imp_sampl += fx_mc_imp_sampl;
      }

     
    //!¤!"%#¤!#%FFFFFF*******K! Please compile! 
     int_MC_imp_sampl = int_MC_imp_sampl/((double)S);
     sum_sigm = sum_sigm/((double) S);
     double variance = sum_sigm-int_MC_imp_sampl*int_MC_imp_sampl;
     double stand_div = (4.*pow(PI,4)/1024.)*sqrt(variance);
     gettimeofday(&end, NULL);
    
     double result_int = 4.*pow(PI,4)*int_MC_imp_sampl/1024. ;
     ofile.open(filename, ios::app);
     ofile <<"Monte Carlo smart sampling"<<endl;
     ofile <<"Number of integration points = "<<S<<endl;
     ofile <<"Integral = " << result_int << endl;
     ofile <<"Variance = " <<variance<< endl;
     ofile <<"ST_div = " <<stand_div<< endl;
     //ofile <<"Time Monte Carlo brute fource:"<<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;
     ofile <<"Time Monte Carlo smart sampling: "<<((end.tv_sec  - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6 << endl;
     ofile << "Error = " << (abs(result_int - calc_val)/calc_val)*100 << endl;
     ofile <<"================================="<<endl;
     ofile.close();

    return 0;

}


////////////////   FUNCTIONS BEGIN //////////////////


       /*
       ** The function 
       **              gauleg()
       ** takes the lower and upper limits of integration x1, x2, calculates
       ** and return the abcissas in x[0,...,n - 1] and the weights in w[0,...,n - 1]
       ** of length n of the Gauss--Legendre n--point quadrature formulae.
       */

void gauleg(double x1, double x2, double x[], double w[], int n)
{
   int         m,j,i;
   double      z1,z,xm,xl,pp,p3,p2,p1;
   double      const  pi = 3.14159265359;
   double      *x_low, *x_high, *w_low, *w_high;

   m  = (n + 1)/2;                             // roots are symmetric in the interval
   xm = 0.5 * (x2 + x1);
   xl = 0.5 * (x2 - x1);

   x_low  = x;                                       // pointer initialization
   x_high = x + n - 1;
   w_low  = w;
   w_high = w + n - 1;

   for(i = 1; i <= m; i++) {                             // loops over desired roots
      z = cos(pi * (i - 0.25)/(n + 0.5));

           /*
           ** Starting with the above approximation to the ith root
           ** we enter the mani loop of refinement bt Newtons method.
           */

      do {
         p1 =1.0;
         p2 =0.0;

           /*
           ** loop up recurrence relation to get the
           ** Legendre polynomial evaluated at x
           */

         for(j = 1; j <= n; j++) {
            p3 = p2;
            p2 = p1;
            p1 = ((2.0 * j - 1.0) * z * p2 - (j - 1.0) * p3)/j;
         }

           /*
           ** p1 is now the desired Legrendre polynomial. Next compute
           ** ppp its derivative by standard relation involving also p2,
           ** polynomial of one lower order.
           */

         pp = n * (z * p1 - p2)/(z * z - 1.0);
         z1 = z;
         z  = z1 - p1/pp;                   // Newton's method
      } while(fabs(z - z1) > ZERO);

          /* 
          ** Scale the root to the desired interval and put in its symmetric
          ** counterpart. Compute the weight and its symmetric counterpart
          */

      *(x_low++)  = xm - xl * z;
      *(x_high--) = xm + xl * z;
      *w_low      = 2.0 * xl/((1.0 - z * z) * pp * pp);
      *(w_high--) = *(w_low++);
   }
} // End_ function gauleg()

void gauss_laguerre(double *x, double *w, int n, double alf)
{
        int i,its,j;
        double ai;
        double p1,p2,p3,pp,z,z1;

        for (i=1;i<=n;i++) {
                if (i == 1) {
                        z=(1.0+alf)*(3.0+0.92*alf)/(1.0+2.4*n+1.8*alf);
                } else if (i == 2) {
                        z += (15.0+6.25*alf)/(1.0+0.9*alf+2.5*n);
                } else {
                        ai=i-2;
                        z += ((1.0+2.55*ai)/(1.9*ai)+1.26*ai*alf/
                                (1.0+3.5*ai))*(z-x[i-2])/(1.0+0.3*alf);
                }
                for (its=1;its<=MAXIT;its++) {
                        p1=1.0;
                        p2=0.0;
                        for (j=1;j<=n;j++) {
                                p3=p2;
                                p2=p1;
                                p1=((2*j-1+alf-z)*p2-(j-1+alf)*p3)/j;
                        }
                        pp=(n*p1-(n+alf)*p2)/z;
                        z1=z;
                        z=z1-p1/pp;
                        if (fabs(z-z1) <= EPS) break;
                }
                if (its > MAXIT) cout << "too many iterations in gaulag" << endl;
                x[i]=z;
                w[i] = -exp(gammln(alf+n)-gammln((double)n))/(pp*n*p2);
        }
}
// end function gaulag

double gammln( double xx)
{
        double x,y,tmp,ser;
        static double cof[6]={76.18009172947146,-86.50532032941677,
                24.01409824083091,-1.231739572450155,
                0.1208650973866179e-2,-0.5395239384953e-5};
        int j;

        y=x=xx;
        tmp=x+5.5;
        tmp -= (x+0.5)*log(tmp);
        ser=1.000000000190015;
        for (j=0;j<=5;j++) ser += cof[j]/++y;
        return -tmp+log(2.5066282746310005*ser/x);
}



//  this function defines the function to integrate in dekart coordinates
double int_function_orig(double x1, double y1, double z1, double x2, double y2, double z2)
{
   double alpha = 2.;
// evaluate the different terms of the exponential
   double exp1=-2*alpha*sqrt(x1*x1+y1*y1+z1*z1);
   double exp2=-2*alpha*sqrt(x2*x2+y2*y2+z2*z2);
   double deno=sqrt(pow((x1-x2),2)+pow((y1-y2),2)+pow((z1-z2),2));
// Cheating!!
  if(deno <pow(10.,-6.)) { return 0;}
  else return exp(exp1+exp2)/deno;
} // end of function to evaluate


/* bad idea
//  this function defines the function to integrate for Monte Carlo method with var substitution
double int_function(double x1, double y1, double z1, double x2, double y2, double z2, double a, double b)
{
   double alpha = 2.;
// evaluate the different terms of the exponential
   double exp1=-2*alpha*sqrt(pow((x1-a),2)+pow((y1-a),2)+pow((z1-a),2))/(b-a);
   double exp2=-2*alpha*sqrt(pow((x2-a),2)+pow((y2-a),2)+pow((z2-a),2))/(b-a);
   double deno=sqrt(pow((x1-x2),2)+pow((y1-y2),2)+pow((z1-z2),2));
  if(deno <pow(10.,-6.)) { return 0;}
  else return exp(exp1+exp2)/deno;
} // end of function to evaluate
*/


//  this function defines the function to integrate in spherical coordinates
double int_function_sphere(double r_subst1, double thetha1, double fi1, double r_subst2, double thetha2, double fi2)
{
// evaluate the different terms of the exponential

   double c=cos(thetha1)*cos(thetha2)+sin(thetha1)*sin(thetha2)*cos(fi1-fi2);
   double denom=sqrt(r_subst1*r_subst1+r_subst2*r_subst2-2.*r_subst1*r_subst2*c);
   if(denom <pow(10.,-6.)) { return 0;}
  else return sin(thetha1)*sin(thetha2)/denom;
} // end of function to evaluate

//  this function defines the function to integrate in spherical coordinates for Monte Carlo importance sampling
double fx_sphere_mc_imp_sampl(double r_subst1, double thetha1, double fi1, double r_subst2, double thetha2, double fi2)
{
// evaluate the different terms of the exponential

   double c=cos(thetha1)*cos(thetha2)+sin(thetha1)*sin(thetha2)*cos(fi1-fi2);
   double denom=sqrt(r_subst1*r_subst1+r_subst2*r_subst2-2.*r_subst1*r_subst2*c);
   if(denom <pow(10.,-6.)) { return 0;}
   else return pow(r_subst1,2)*pow(r_subst2,2)*sin(thetha1)*sin(thetha2)/denom;
} // end of function to evaluate

